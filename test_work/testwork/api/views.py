from rest_framework.generics import GenericAPIView, RetrieveUpdateAPIView
from .serializers import UserSerializers, UpdateUserSerializer
from rest_framework import response
from rest_framework import permissions
from users.models import User
# Create your views here.


class CreateUserApiView(GenericAPIView):
    serializer_class = UserSerializers

    def post (self,request,*args,**kwargs):
        serializers = self.get_serializer(data=request.data)
        if serializers.is_valid():
            serializers.save()
            return response.Response(serializers.data)
        return response.Response(serializers.errors)


class DetailUserAPIView(RetrieveUpdateAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = UpdateUserSerializer
    queryset = User
    lookup_field = 'id'
    lookup_url_kwarg = 'user_id'

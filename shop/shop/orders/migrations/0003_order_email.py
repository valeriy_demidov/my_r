# Generated by Django 2.1.4 on 2019-01-09 08:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_auto_20190109_1157'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='email',
            field=models.EmailField(blank=True, help_text='user@mail.com', max_length=254, null=True, verbose_name='Электронная почта'),
        ),
    ]

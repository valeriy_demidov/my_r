from django.views.generic import UpdateView, DetailView, DeleteView
from django.urls import reverse_lazy

from products.models import Book
from .models import ProductsInCart, Cart
from .forms import AddProductForm
from orders.forms import CheckoutOrderForm
from reference.models import OrderStatus


new_order_status = OrderStatus.objects.get(pk=1)

class AddProductToCartView(UpdateView):
    model = ProductsInCart
    template_name = 'cart/add-product.html'
    form_class = AddProductForm

    def get_success_url(self):
        print(self.request.POST.get('next', '/'), 'next')
        return self.request.POST.get('next', '/')

    def get_object(self):
        """
        Ек
        """
        cart_id = self.request.session.get('cart_id')
        product_id = self.kwargs.get('product')
        cart, cart_created = Cart.objects.get_or_create(
            pk=cart_id,
            defaults={
                'user': self.request.user
            }
        )
        product = Book.objects.get(pk=product_id)
        product_in_cart, created = ProductsInCart.objects.get_or_create(
            cart=cart,
            product=product,
            defaults={
                'cart': cart,
                'product': product,
                'quantity': 1},
        )
        if cart_created:
            self.request.session['cart_id'] = cart.pk
        elif not created:
            product_in_cart.quantity += 1
        return product_in_cart

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["next"] = self.request.GET.get('next', '/')
        return context


class CartView(DetailView):
    model = Cart
    template_name = 'cart/view-cart.html'

    def get_object(self):
        cart_id = self.request.session.get('cart_id')
        cart, cart_created = Cart.objects.get_or_create(
            pk=cart_id,
            defaults={
                'user': self.request.user
            }
        )
        return cart

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        checkot_form = CheckoutOrderForm()
        checkot_form.fields['cart'].initial = self.object
        checkot_form.fields['status'].initial = new_order_status
        context["form"] = checkot_form
        return context


class DeleteItemDeleteView(DeleteView):
    model = ProductsInCart
    template_name = 'cart/delete-item.html'

    def get_success_url(self):
        return self.request.POST.get('next', '/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["next"] = self.request.GET.get('next', '/')
        return context
from django.urls import path

from . import views

app_name = "cart"

urlpatterns = [
    path(
        'add/<int:product>/',
        views.AddProductToCartView.as_view(),
        name="add-product-to-cart"),
    path(
        'view/',
        views.CartView.as_view(),
        name="view"),
    path(
        'delete/<int:pk>',
        views.DeleteItemDeleteView.as_view(),
        name="delete"),
]
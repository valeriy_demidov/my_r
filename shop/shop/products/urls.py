from django.urls import path
from . import views
from .views import ListBookView,CreateBookView,UpdateBookView,DetailBookView,DeleteBookView
from . import views


urlpatterns = [
    path('',ListBookView.as_view(),name='list_book'),
    path('create/book/',CreateBookView.as_view(),name='create_book'),
    path('update/book/<int:pk>/',UpdateBookView.as_view(),name='update_book'),
    path('delete/book/<int:pk>/',DeleteBookView.as_view(),name = 'delete_book'),
    path('detail/book/<int:pk>/',DetailBookView.as_view(),name = 'detail_book'),
]
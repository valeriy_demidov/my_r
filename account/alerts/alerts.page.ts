import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';

import {AccountService, PagerService} from '../../../service/index'
import {Advert} from '../../../models/'
import {AlertModalComponent} from "../../../components/index";
import {error} from "util";
import {Subscription} from "rxjs";
import {CommonService} from "../../../service/common.service";


@Component({
    selector: 'alerts-page',
    templateUrl: 'alerts.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class AlertsPage implements OnInit {
    public closeDeleteAlertSubscription: Subscription;
    public deleteAlertSubscription: Subscription;
    private alerts: any[];
    private pager: any = {};
    private currentPage: number = 1;
    private preloader: boolean = true;
    private resultCounter: number = 0;
    constructor(private modalService: NgbModal,
                private accountService: AccountService,
                private toastrService: ToastrService,
                private pagerService: PagerService,
                private commonService: CommonService) {
        this.closeDeleteAlertSubscription = this.commonService.closePopoverDeleteAlertIdChange.subscribe(
            (data) => {
                this.alerts.forEach((alert) => {
                    if (alert['id'] == data) {
                        alert['isShowDeletePopover'] = false;
                    }
                })
            }
        )
        this.deleteAlertSubscription = this.commonService.deleteAlertChange.subscribe(
            (data) => {
                this.deleteAlert(data);
            }
        )
    }


    ngOnInit() {
        this.getAlerts();

    }

    private getAlerts() {
        this.preloader = true;
        this.accountService.getAlerts().subscribe((data) => {
            this.alerts = data.json().results;
            this.alerts.forEach((alert) => {
                alert['isShowDeletePopover'] = false;
            })
            this.resultCounter = data.json().count;
            this.preloader = false;
            this.setPage(this.currentPage, false)
        }, (error) => {

        })
    }

    public openAlertModal() {
        const modalRef = this.modalService.open(AlertModalComponent, {windowClass: 'auth-modal'});
        modalRef.result.then(()=> {
            this.currentPage = 1;
            this.getAlerts()
        });
    }

    private deleteAlert(id: string) {
        this.accountService.deleteAlert(id).subscribe(()=> {
            this.getAlerts();
            this.currentPage = 1;
            this.toastrService.success('Success!', 'Alert deleted');
        })
    }

    public changeAlertIsShowDeletePopover(alert: any) {
        alert['isShowDeletePopover'] = !alert['isShowDeletePopover'];
    }

    public setPage(page: number, queried: boolean = true) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        this.pager = this.pagerService.getPager(this.resultCounter, page);
        if (queried) {
            this.currentPage = page;
            this.getAlerts();
        }


    }


}

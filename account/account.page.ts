import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {UserService} from "../../service/user.service";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {Region, Country} from "../../models/region";
import {RegionService} from "../../service/region.service";
import {User} from "../../models/user";
import {Subscription} from "rxjs";
import * as _ from 'underscore'


@Component({
    selector: 'account-page',
    templateUrl: 'account.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class AccountPage implements OnInit {

    subscription: Subscription;
    user: User = new User();
    photoUploader: any;

    constructor(private userService: UserService,
                private toastrService: ToastrService) {
        this.subscription = this.userService.userChange.subscribe((data) => {
            this.user = data;
        });

    }


    ngOnInit() {
        this.userService.updateCurrentUserAccount();
        this.user = this.userService.currentUser;
        //this.userService.updateUserPhoto('').subscribe(data => console.log(data));
    }

    private updatePhoto(event:any) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            if (!file.type.startsWith('image')){
                this.toastrService.error('Error','Bad type of file')
                return
            }
            this.userService.updateUserPhoto(file, this.user.id).subscribe((data) => {
                this.toastrService.success("Success", "Your photo was updated");
                this.userService.getCurrentUserAccount().subscribe((data) => {
                    this.userService.setCurrentUser(data.json())
                });
            });
        }
    }


}

import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ToastrService} from "ngx-toastr";
import {Router, ActivatedRoute} from "@angular/router";
import {Http} from '@angular/http';

@Component({
    selector: 'page-item',
    templateUrl: 'reset.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class PasswordResetPage implements OnInit {
    private baseUrl: string = process.env['API_URL'];
    private uid: string;
    private key: string;

    public securityForm = this.formBuilder.group({
        new_password: ['', [Validators.required]],
        repeat_password: ['', [Validators.required]],
    });


    constructor(private router: Router,
                private route: ActivatedRoute,
                private formBuilder: FormBuilder,
                private http: Http,
                private toastrService: ToastrService,) {}


    ngOnInit() {
         this.route.params.subscribe(params => {
            this.uid = params['uid'];
            this.key = params['token'];
        });
    }

    public updatePassword(){
        if (this.securityForm.value['new_password']!==this.securityForm.value['repeat_password']){
            return this.toastrService.error('Error!', 'New passwords should be same!');
        }
        this.http.post(this.baseUrl + 'account/password-reset/',{uid:this.uid,token:this.key,password:this.securityForm.value['new_password']}).subscribe((data) => {
            this.toastrService.success('Success!', 'Password reset!');
        }, (error) => {
            this.toastrService.error('Error!', 'Token expired, sent email again');
        })
        this.router.navigate(['/']);
    }
}

import { Component, ViewEncapsulation, OnInit, Input, EventEmitter } from '@angular/core';
import { AdvertService, ModalService } from '../../../../service/index';
import { CurrencyService } from '../../../../service/index';
import { PagerService } from '../../../../service/pager.service';
import { ConverterCurrencyPipe } from '../../../../pipes/index';
import { Currency } from 'angular-l10n';
import { UploadOutput, UploadInput, humanizeBytes } from 'ngx-uploader';
import { NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Manufacturer, MachineModel, Advert, Choices } from '../../../../models/index'
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Country, Region } from '../../../../models/region';
import { CommonService, RegionService, UserService } from '../../../../service/index';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as _ from 'underscore';


import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { User } from "../../../../models/user";
import { ToastrService } from "ngx-toastr";
import {error} from "util";


const uploadURL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'listing-edit-page',
    templateUrl: 'listing-edit.tpl.pug',
    encapsulation: ViewEncapsulation.None,
    providers: [ConverterCurrencyPipe]
})
export class ListingEditPage implements OnInit {

    public dateOptions: INgxMyDpOptions = {
        selectorWidth: '100%',
        selectorHeight: '290px',
        dateFormat: 'dd/mm/yyyy',
    };

    public paypalProperties = {
        env: process.env['PAYPAL_ENV'],
        client: {
            sandbox: process.env['PAYPAL_SANDBOX_CLIENT_ID'],
            production: process.env['PAYPAL_PRODUCTION_CLIENT_ID']
        },
        commit: true,
        payment: ((data: any, actions: any) => {
            this.preloader = true;
            return this.submitAdvert().then((advert => {
                    this.preloader = false;
                    this.edit = true;
                    this.advert.id = advert['id'];
                    return this.advertService.updateListingOptions(
                        advert['id'],
                        this.stepFourForm.value
                    ).toPromise().then((listingOptions: any) => {
                        let paypalPaymentId = listingOptions['paypal_payment_id'];
                        if (!paypalPaymentId) {
                            return Promise.reject('Invalid payment id');
                        }
                        return paypalPaymentId;
                    }, (err) => {
                        console.error('Cannot update listing option', err);
                        return Promise.reject('Cannot update listing option');
                    });
                })
            ).catch(
                (err) => {
                    this.preloader = false;
                    return Promise.reject(err);
                });
        }),

        onAuthorize: ((data: any, actions: any) => {
            this.preloader = true;
            this.advertService.confirmPayment(data.paymentID, data.payerID).finally(
                () => this.preloader = false
            ).subscribe(() => {
                this.router.navigate(['/account/my_listings']);
            });
        }),
        onCancel: ((data: any, actions: any) => {

        }),
        style: {
            color: 'blue', // orange, blue, silver
            shape: 'rect'    // pill, rect
        }
    };


    @Currency() currency: string;
    currentCurrency: string;
    price: number;
    files: Array<any> = [];
    uploadInput: EventEmitter<UploadInput>;
    humanizeBytes: Function;
    preloader = false;
    dragOver: boolean;
    advert_id: number;
    currentStep: number = 1;
    advert: Advert = new Advert();
    listingOptionsCurrency: string;
    advertVideoLinks: string[] = [];
    listingOptions: any[] = [];
    manufactures: Manufacturer[] = [];
    countries: Country[] = [];
    countryIsValid = false;
    regions: Region[] = [];
    machineModels: MachineModel[] = [];
    conditionChoices: Choices[] = [];
    termsChoices: Choices[] = [];
    currencyChoices: Choices[] = [];
    operationChoices: Choices[] = [];
    advertTypeChoices: Choices[] = [];
    years: number[] = [];
    showError: boolean = false;
    stepOneForm: FormGroup;
    stepTwoForm: FormGroup;
    stepThreeForm: FormGroup;
    stepFourForm: FormGroup = new FormGroup({});
    edit: boolean = false;
    mainImageStatus: boolean = false;

    LISTING_OPTIONS_ICON_CLASSES = {
        'standart': 'sign--standart',
        'urgent': 'sign--urgent',
        'featured': 'sign--featured',
        'spotlight': 'sign--spotlight',
        'newsletter': 'sign--newsletter',
    };

    listingPrice = 0;
    isHideCountry: boolean = false;
    paypalLoaded: boolean = false;
    currentUser: User;
    GB: string = 'GB';
    pricingVAT = 0;

    constructor(private fb: FormBuilder,
                private advertService: AdvertService,
                private pagerService: PagerService,
                private regionService: RegionService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private currencyService: CurrencyService,
                private userService: UserService,
                private commonService: CommonService,
                private modalService: ModalService,
                private converterCurrency: ConverterCurrencyPipe,
                private toastrService: ToastrService) {
        this.humanizeBytes = humanizeBytes;
        this.currentCurrency = this.currencyService.getCurrency();
        this.advert.currency = this.currentCurrency;
        this.uploadInput = new EventEmitter<UploadInput>()
    }

    loadMachineModels() {
        this.machineModels = [];
        this.advert.model_id = null;
        this.advertService.getMachinesModels(this.advert.manufacturer_id).subscribe(
            success => {
                this.machineModels = success.json();
            }
        );
    }

    onPriceChange() {
        this.advert.price = this.price;
    }

    updatePrice() {
        this.onPriceChange();
    }

    addVideoLink() {
        const arrayControl = <FormArray> this.stepTwoForm.controls['advertVideoArray'];
        let newGroup = this.fb.group({
            video: ['', null],
        });
        arrayControl.push(newGroup);
    }

    removeVideoLink(index: number) {
        // this.advertVideoLinks.splice(index, 1);
        const arrayControl = <FormArray> this.stepTwoForm.controls['advertVideoArray'];
        arrayControl.removeAt(index);
    }

    checkOneFormValid() {
        let htmlMessage: string = 'The following fields are missing:';

        let fields: Array<any> = [
            {'name': 'Manufacturer', 'field_name': 'manufacturer_id'},
            {'name': 'Model', 'field_name': 'model_id'},
            {'name': 'Pre-owned', 'field_name': 'advert_type'},
            {'name': 'Condition', 'field_name': 'condition'},
            {'name': 'Production', 'field_name': 'operation'}
        ];

        for (let field of fields) {
            if (!this.stepOneForm.controls[field.field_name].valid) {
                htmlMessage += `<li>${field.name}</li>`;
            }
        }

        this.toastrService.warning(htmlMessage, '', {enableHtml: true});
    }

    stepOneSubmit() {
        window.scrollTo(0, 0);
        if (this.stepOneForm.valid) {
            this.showError = false;
            this.currentStep = 2;
        } else {
            this.showError = true;
            this.checkOneFormValid();
        }
    };

    back() {
        window.scrollTo(0, 0);
        if (this.currentStep > 1) {
            this.currentStep -= 1;
        }
    }


    stepTwoSubmit() {
        window.scrollTo(0, 0);
        if (this.stepTwoForm.valid && this.checkTwoFormValid()) {
            this.advertVideoLinks = [];
            let advertVideoArray = this.stepTwoForm.value['advertVideoArray'];
            if (advertVideoArray) {
                for (let item of this.stepTwoForm.value['advertVideoArray']) {
                    if (item['video']) {
                        this.advertVideoLinks.push(item['video']);
                    }
                }
            }
            this.currentStep = 3;
        }
    };

    checkTwoFormValid() {
        if (this.stepTwoForm.value['advertVideoArray']) {
            for (let item of this.stepTwoForm.value['advertVideoArray']) {
                if (item['video'] && !this.validateYouTubeUrl(item['video'])) {
                    let htmlMessage: string = 'Link to youtube video is not correct!';
                    this.toastrService.warning(htmlMessage, '', {enableHtml: true});
                    return false;
                }
            }
        }
        return true;
    }

    checkThreeFormValid() {
        let htmlMessage: string = 'The following fields are missing:';

        let fields: Array<any> = [
            {'name': 'Availability', 'field_name': 'availability'},
            {'name': 'Shipment', 'field_name': 'terms'},
        ];

        for (let field of fields) {
            if (!this.stepThreeForm.controls[field.field_name].valid) {
                htmlMessage += `<li>${field.name}</li>`;
            }
        }

        if (!this.advert.country) {
            htmlMessage += '<li>Location</li>';
            this.countryIsValid = true;
        } else {
            this.countryIsValid = false;
        }

        this.toastrService.warning(htmlMessage, '', {enableHtml: true});
    }

    stepThreeSubmit() {
        window.scrollTo(0, 0);
        if ( this.advert.region && !this.advert.country){
            this.isHideCountry = true
        }
        if (this.stepThreeForm.valid && this.isHideCountry && this.advert.region) {
            this.currentStep = 4;
            this.showError = false;
        }
        else if (this.stepThreeForm.valid && !this.isHideCountry && this.advert.country) {
            this.currentStep = 4;
            this.showError = false;
        }
        else {
            this.showError = true;
            this.checkThreeFormValid();
        }
    }

    stepFourSubmit() {
        window.scrollTo(0, 0);
        this.preloader = true;
        return this.submitAdvert().then(() => {
            this.checkImage()
        }).catch(error => {
            this.router.navigate(['/account/my_listings'], {queryParams: error})
        });
    }

    checkImage() {
        if (this.mainImageStatus){
            this.preloader = false;
            this.router.navigate(['/account/my_listings']).then(()=>{
                this.advertService.sendAlerts(this.advert_id, this.advert.model_id).subscribe((response)=>{
                    console.log(response.json())
                });
            });
        }else{
            setTimeout(()=>{
                this.checkImage();
            }, 5000);
        }
    }

    convertCommaSeparatedToInt() {
        let total_cost = '';
        total_cost += this.advert.total_cost;
        total_cost = total_cost.replace(new RegExp(this.commonService.THOUSANDS_SEPARATOR, 'g'), '');
        this.advert.total_cost = parseInt(total_cost);

        let price = '';
        this.onPriceChange();
        price += this.advert.price;
        price = price.replace(new RegExp(this.commonService.THOUSANDS_SEPARATOR, 'g'), '');
        this.advert.price = parseInt(price);
    };

    submitAdvert() {
        this.convertCommaSeparatedToInt();
        return new Promise((resolve, reject) => {
            if (this.stepFourForm.valid) {
                this.showError = false;
                this.convertDate();
                this.advert = Object.assign(this.advert, this.stepFourForm.value);

                this.advert.videos = [];

                for (let advertVideoLink of this.advertVideoLinks) {
                    this.advert.videos.push({
                        'video': advertVideoLink
                    });
                }
                if (this.files.length==0){
                    this.mainImageStatus = true;
                }
                if (!this.edit) {
                    resolve(this.advertService.addAdvert(this.advert).toPromise().then(
                        (data) => {
                            for (let [index, file] of this.files.entries()) {
                                this.advertService.createAdvertImage(file.result, data.json().id, index.toString()).subscribe(()=>{
                                    if (index == 0){
                                        this.mainImageStatus = true;
                                    }
                                });
                            }
                            this.advert_id = data.json().id
                            return data.json();
                        }
                    ).catch(error => {
                        throw error.json()
                    }));
                } else {
                    resolve(this.advertService.updateAdvert(this.advert).toPromise().then(
                        (data) => {
                            for (let [index, file] of this.files.entries()) {
                                if (!file.id) {
                                    this.advertService.createAdvertImage(file.result, data.json().id, index.toString()).subscribe(()=>{
                                        if (index == 0){
                                            this.mainImageStatus = true;
                                        }
                                    });
                                }
                                else {
                                    this.advertService.updateAdvertImage(file.id, {order: index}).subscribe(()=>{
                                        if (index == 0){
                                            this.mainImageStatus = true;
                                        }
                                    });
                                }
                            }
                            return data.json();
                        }
                    ));
                }
            } else {
                this.showError = true;
                reject('invalid form');
            }
        });
    }

    ngOnInit() {
        this.currentUser = this.userService.currentUser;

        if (!this.currentUser) {
            this.router.navigate(['/']);
        }
        this.getAdvertId();
        this.advertService.getManufacturers().subscribe(
            success => {
                this.manufactures = success.json();
            }
        );
        this.advertService.getAdvertChoices().subscribe(
            success => {
                let choices = success.json();
                this.conditionChoices = choices.condition;
                this.termsChoices = choices.terms;
                this.operationChoices = choices.operation;
                this.advertTypeChoices = choices.advert_type;
                this.currencyChoices = choices.currencies;
                this.years = choices.years;
                this.listingOptions = choices.listings;
                this.listingOptionsCurrency = '';
                let formGroup = {};
                for (let listingOption of this.listingOptions) {
                    listingOption.iconClass = this.LISTING_OPTIONS_ICON_CLASSES[listingOption.name];
                    this.listingOptionsCurrency = listingOption.currency;

                    this.stepFourForm.addControl(listingOption.name, new FormControl({
                        value: (listingOption.price === 0),
                        disabled: (listingOption.price === 0),
                    }, Validators.required));
                    this.stepFourForm.addControl(listingOption.name + '_pending', new FormControl(
                        listingOption.choices[0][0], Validators.required
                    ));

                }

            }
        );

        this.regionService.getRegions().subscribe(
            (result) => {
                this.regions = result.json();
            }
        );

        this.regionService.getCountries().subscribe(
            (result) => {
                this.countries = result.json();
            }
        );


        this.stepOneForm = this.fb.group({
            manufacturer_id: [null, Validators.compose([Validators.required])],
            model_id: [null, Validators.compose([Validators.required])],
            advert_type: [null, Validators.compose([Validators.required])],
            year: [null, null],
            condition: [null, Validators.compose([Validators.required])],
            operation: [null, Validators.compose([Validators.required])],
            production_count: [null, null],
            details: [null, null],
            serial: [null, null]
        });

        this.processStepTwoForm();

        this.stepThreeForm = this.fb.group({
            price: [null],
            currency: [null, Validators.compose([Validators.required])],
            availability: [null, Validators.compose([Validators.required])],
            terms: [null, Validators.compose([Validators.required])],
            country: [null, Validators.compose([Validators.required])],
            region: [null, null],
            private_notes: [null, null],
            owner_ref: [null, null],
        });
        this.stepFourForm.valueChanges.subscribe((data) => {
            this.getTotalPrice(data);
        });
        this.setStepFromParams();
    }

    setStepFromParams() {
        this.activatedRoute.params.subscribe((params: Params) => {
            if (params['step'] === 'four') {
                this.currentStep = 4;
            }
        });
    }


    public updateLocationFormInput(event: any) {

        let advert = this.advert;
        let countryControl = this.stepThreeForm.controls['country'];
        let regionControl = this.stepThreeForm.controls['region'];

        let country = _.find(this.countries, function (iterItem) {
            return iterItem.iso == advert.country;
        });
        if (typeof event === 'object' && event.target.checked) {
            if (!!this.advert.country) {
                let region = _.find(this.regions, function (iterItem) {
                    return iterItem.id == country.region;
                });
                this.advert.region = region.id.toString();
                regionControl.setValue(region.id);
            }
            countryControl.setValue('');
            countryControl.disable();
        } else {
            regionControl.setValue('');
            countryControl.enable();
        }

        if (typeof event === 'string' && event.length > 0) {
            let region = _.find(this.regions, function (iterItem) {
                return iterItem.id == country.region;
            });
            this.advert.region = region.id.toString();
            regionControl.setValue(region.id);
        }

    }

    public dynamicRequiredRegionValidator(control: FormControl): {[key: string]: boolean} {

        if (!this.isHideCountry) {
            return {};
        }
        //return {required: true};
    }

    processStepTwoForm() {
        this.stepTwoForm = this.fb.group({
            advertVideoArray: this.fb.array([])
        });
        const arrayControl = <FormArray> this.stepTwoForm.controls['advertVideoArray'];
        this.advertVideoLinks.forEach(item => {
            if (!!item) {
                let newGroup = this.fb.group({
                    video: [item, null],
                });
                arrayControl.push(newGroup);
            }
        });
    }

    onUploadOutput(output: UploadOutput): void {

        if (output['type'] === 'allAddedToQueue') { // when all files added in queue
            // console.log('Image -> allAddedToQueue');
        } else if (output['type'] === 'addedToQueue' && typeof output['file'] !== 'undefined') {
            let reader = new FileReader();
            if (!output.file.type.startsWith('image')){
                this.toastrService.error('Error','Bad type of file');
                return
            }
            reader.onload = (event) => {
                this.files.push({rawImage: event.target['result'], result: output['file']});
            };
            reader.readAsDataURL(output['file'].nativeFile);
        }
    }

    removeFile(id: string, index: number, fromBackend: boolean): void {
        if (!!fromBackend) {
            this.advertService.deleteAdvertImage(id).subscribe(
                success => {

                },
                error => {

                }
            )
        }
        else {
            this.uploadInput.emit({type: 'remove', id: id});
        }
        this.files.splice(index, 1);
    }

    startUpload(id: string): void {
        const event: UploadInput = {
            type: 'uploadAll',
            url: 'http://localhost:8000/api/v1/update-advert-photo/',
            method: 'POST',
            data: {id: id}
        };

        this.uploadInput.emit(event);
    }

    convertDate() {
        let date = this.advert['availability_date']['date'];
        if (date) {
            this.advert['availability'] = date.year + '-' + date.month + '-' + date.day;
        }
    }

    public getAdvertId() {
        return this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            if (id !== 'new') {
                this.edit = true;
                this.advertService.getAdvert(id).subscribe((data) => {
                    this.advert = data.json();
                    this.price = data.json().price;
                    this.files = this.advert.images;
                    this.updatePrice();
                    this.loadMachineModels();
                    this.advert.model_id = data.json().model_id;
                    let newDate = new Date(data.json().availability);

                    this.advert['availability_date'] = {
                        date: {
                            day: '',
                            month: '',
                            year: '',
                        }
                    };

                    this.advert['availability_date']['date']['day'] = newDate.getDate();
                    this.advert['availability_date']['date']['month'] = newDate.getMonth() + 1;
                    this.advert['availability_date']['date']['year'] = newDate.getFullYear();

                    this.advert.videos.forEach(iterItem => {
                        this.advertVideoLinks.push(iterItem.video)
                    });
                    this.processStepTwoForm();

                    // ****listing options edit saved values****
                    // for (let listingOption of this.listingOptions) {
                    //     if (!this.stepFourForm.controls[listingOption.name].disabled) {
                    //         this.stepFourForm.controls[listingOption.name].setValue(this.advert[listingOption.name]);
                    //         let pendingValue = this.advert[listingOption.name + '_pending'];
                    //         if (pendingValue) {
                    //             this.stepFourForm.controls[listingOption.name + '_pending'].setValue(
                    //                 pendingValue
                    //             );
                    //         }
                    //
                    //     }
                    // }
                    // ****listing options edit saved values****
                });
            } else {
                this.checkIfUserCanAddListing();
            }
        });
    }

    checkIfUserCanAddListing() {
        if (this.currentUser && !this.currentUser.is_can_add_listing) {
            this.toastrService.warning('Please complete you profile to add listing');
            this.router.navigate(['/account/settings']);
        }
    }


    toggleAllListingOptions() {
        let allChecked = this.isAllListingOptionsChecked();
        for (let listingOption of this.listingOptions) {
            if (listingOption.price) {
                this.stepFourForm.controls[listingOption.name].setValue(!allChecked);
            }
        }
    }

    isAllListingOptionsChecked() {
        return _.every(this.listingOptions, (listingOption) => {
            return this.stepFourForm.controls[listingOption.name].value;
        });
    }

    getTotalPrice(formData: any) {
        let price = 0;
        let decimalScaleValue = 100;
        for (let listingOption of this.listingOptions) {
            let name = listingOption.name;
            let enabled = formData[name];
            if (listingOption.price && enabled) {
                let pending = formData[name + '_pending'];
                price += listingOption.price * 1 * decimalScaleValue * pending;
            }
        }
        this.listingPrice = price / decimalScaleValue;
        this.pricingVAT = 0;

        if (this.listingPrice > 0) {
            let _this = this;
            _this.paypalLoaded = false;
            setTimeout(function () {
                _this.paypalLoaded = true
            }, 3000);
            if (this.currentUser && this.currentUser.country == this.GB) {
              this.getVat()
            }
        }
    }

    getVat() {
        this.pricingVAT = Math.floor(this.listingPrice * 0.2* 100) / 100
    }

    getResultPrice() {
        return this.pricingVAT + this.listingPrice
    }

    cancel() {
        this.router.navigate(['/account/my_listings']);
    }

    validateYouTubeUrl(url: string) {
        if (url != undefined || url != '') {
            let regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
            let match = url.match(regExp);
            if (match && match[2].length == 11) {
               return true;
            } else {
                return false;
            }
        }
    }

    openPreview(choice: any) {
        const src = `../../../../../assets/img/advert_preview/${choice['name']}.png`;
        this.modalService.openImageShowModal(src);
    }
}

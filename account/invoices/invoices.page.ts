import { ActivatedRoute, Params } from '@angular/router';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { UserService, AccountService, PagerService } from '../../../service/index';
import {User} from "../../../models/index";
import * as moment from 'moment';
import {CurrencyPipe} from "@angular/common";
import {CurrencyService} from "../../../service/index";

@Component({
    selector: 'invoices-page',
    templateUrl: 'invoices.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class InvoicesPage implements OnInit {

    user: User = new User();
    private pager: any = {};
    private invoices: any = [];
    private currentPage: number = 1;
    private preloader: boolean = true;
    private resultCounter: number = 0;

    constructor(private accountService: AccountService,
                private pagerService: PagerService,
                private currencyService: CurrencyService,
                private activatedRoute: ActivatedRoute,
                private currencyPipe: CurrencyPipe,
                private userService: UserService) {
        this.user = this.userService.currentUser;
    }

    ngOnInit() {
        this.getInvoices();
        this.checkParams();
    }

    private getInvoices() {
        this.preloader = true;
        this.accountService.getInvoices().subscribe((invoices) => {
            this.preloader = false;
            this.invoices = invoices.json().results;
            // this.invoices.forEach((iterItem: any) => {
            //     this.currencyService.getExchangeRateFromAPI('EUR', 'GBP').subscribe(
            //         (data) => {
            //             let exchangeRate = data.json().rates['EUR'];
            //             let EURAmount = Math.round(iterItem.amount * exchangeRate * 100) / 100;
            //             iterItem.EURAmount = EURAmount
            //         },
            //         (err) => {
            //
            //             console.log(err.json());
            //
            //         }
            //     );
            // });
            this.resultCounter = invoices.json().count;
            this.setPage(this.currentPage, false);

        }, (error) => {

        });
    }

    public setPage(page: number, queried: boolean = true) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        this.pager = this.pagerService.getPager(this.resultCounter, page);
        if (queried) {
            this.currentPage = page;
            this.getInvoices();
        }
    }

    private openPDF(invoiceId: number) {
        this.accountService.getInvoice(invoiceId).subscribe((invoice) => {
            this.createIncoivePDF(invoice.json());
        }, (error) => {

        });
    }

    showVatForGB(invoice: any) {
        return (invoice.user.country == 'GB') ? '20' : '0';
    }

    getPriceWith2Decimals(price: any) {
        return price.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    }

    capitalizeFirstLetter(stringToUpdate: string) {
        return stringToUpdate.charAt(0).toUpperCase() + stringToUpdate.slice(1);
    }

    private createIncoivePDF(invoice: any) {
        let vatAmount: any = parseFloat(invoice.vat);
        let amountWithVat: any = parseFloat(invoice.amount);
        let amountWithoutVat: any = parseFloat(invoice.amount_without_vat);


        if (invoice.currency) {
            // let currencySymbol: any = this.currencyPipe.transform(0, invoice.currency, true, "1.0-2");
            // currencySymbol = currencySymbol.replace(/[0-9]/g, '');
            // vatAmount = currencySymbol + this.getPriceWith2Decimals(vatAmount);
            // amountWithoutVat = currencySymbol + this.getPriceWith2Decimals(amountWithoutVat);
            amountWithVat = this.currencyPipe.transform(amountWithVat, invoice.currency, true);
            amountWithoutVat = this.currencyPipe.transform(amountWithoutVat, invoice.currency, true);
            vatAmount = this.currencyPipe.transform(vatAmount, invoice.currency, true);
        }

        let jsPDF = require('jspdf');
        require('jspdf-autotable');

        let doc = new jsPDF();

        let columns: any = ['Product/Service', 'Currency', 'Vat', 'Rate', 'Total'];
        let machine_name: any = (invoice.advert_name) ? invoice.advert_name : '';
        let client_vat_id: any = (invoice.user.vat) ? invoice.user.vat : '';

        let rows: any = [
            [
                invoice.vat,
                invoice.amount,
                invoice.currency,
                // machine_name,
                parseFloat(invoice.amount) + parseFloat(invoice.vat)
            ]
        ];

        let date = new Date;

        if (invoice.paid_date) {
            date = this.dateFormat(invoice.paid_date, 'DD, MMMM, YYYY');
        }

        //printreef Logo
        doc.setFontSize(30);
        doc.setFont('arial');
        doc.setTextColor(0, 167, 220);
        doc.setFontType('bold');
        doc.text(141, 27, 'print');
        doc.setFontSize(30);
        doc.setTextColor(0, 0, 0);
        doc.setFontType('bold');
        doc.text(165, 27, 'reef');
        doc.setFontSize(8);
        doc.text(152, 32, 'Buy, sell and trade');
        doc.text(152, 36, 'printing equipment');

        //details
        doc.setTextColor(65, 65, 65);
        doc.setFontSize(12);
        doc.setFontType('bold');
        doc.text(25, 25, 'Pressfinders Limited');
        doc.setFontType('normal');
        doc.text(25, 31, 'Suite A, 10th Floor');
        doc.text(25, 37, 'Maple House');
        doc.text(25, 43, 'High Street');
        doc.text(25, 49, 'Potters Bar');
        doc.text(25, 55, 'Herts');
        doc.text(25, 61, 'EN6 5BS');

        //company and vat number
        doc.setFontType('bold');
        doc.text(25, 73, 'COMPANY NUMBER');
        doc.setFontType('normal');
        doc.text(68, 73, '4995516');

        doc.setFontType('bold');
        doc.text(25, 78, 'VAT NUMBER');
        doc.setFontType('normal');
        doc.text(55, 78, 'GB 843 2259 33');

        //email and phone
        doc.setFontType('bold');
        doc.text(25, 90, 'Email');
        doc.setFontType('normal');
        doc.text(38, 90, 'support@printreef.com');

        doc.setFontType('bold');
        doc.text(25, 95, 'Web');
        doc.setFontType('normal');
        doc.text(38, 95, 'www.printreef.com');


        //table
        doc.setFillColor(120, 170, 180);
        doc.rect(25, 112, 160, 9, 'F');
        doc.setTextColor(255, 255, 255);
        doc.text(27, 118, 'INVOICE NUMBER: ' + invoice.id);
        if (invoice.paid_date) {
            doc.text(143, 118, date);
        }

        //BILL TO
        doc.setTextColor(120, 170, 180);
        doc.text(27, 131, 'BILL TO');
        doc.setDrawColor(120, 170, 180);
        doc.setLineWidth(0.25);
        doc.line(25, 133, 185, 133);
        doc.setTextColor(65, 65, 65);
        doc.text(27, 142, invoice.user.email);

        //CLIENT VAT ID
        doc.setTextColor(120, 170, 180);
        doc.text(27, 151, 'VAT');
        doc.setDrawColor(120, 170, 180);
        doc.setLineWidth(0.25);
        doc.line(27, 153, 185, 153);
        doc.setTextColor(65, 65, 65);
        doc.text(27, 160, client_vat_id);
        doc.setFillColor(120, 170, 180);
        doc.rect(25, 163, 160, 9, 'F');
        doc.setTextColor(255, 255, 255);

        //Qty Section
        doc.text(27, 169, 'Qty.');
        doc.text(43, 169, 'DESCRIPTION');
        doc.text(144, 169, '(ex. VAT)');

        doc.setFontSize(10);
        doc.setTextColor(65, 65, 65);
        let currentHeight : number = 179;

        if (invoice.invoice_items.length) {
            invoice.invoice_items.forEach((item: object, index: number) => {
                let itemAmount: any = item['amount']
                if (invoice.currency) {
                    itemAmount = this.currencyPipe.transform(itemAmount, invoice.currency, true);
                }
                doc.text(27, currentHeight, '1')
                doc.text(43, currentHeight, `${this.capitalizeFirstLetter(item['name'])} advert for ${machine_name}`);
                doc.text(171, currentHeight, itemAmount);
                doc.setDrawColor(206, 215, 230);
                doc.setLineWidth(0.25);
                currentHeight += 4;
                doc.line(25, currentHeight, 185, currentHeight);
                currentHeight += 6;
            })
        } else {
            doc.text(27, currentHeight, '1')
            doc.text(43, currentHeight, 'Payment advert for ' + machine_name);
            doc.text(171, currentHeight, amountWithoutVat);
            doc.setDrawColor(206, 215, 230);
            doc.setLineWidth(0.25);
            currentHeight += 4
            doc.line(25, currentHeight, 185, currentHeight);
        }


        currentHeight += 9;

        doc.setDrawColor(120, 170, 180);
        doc.setLineWidth(0.25);
        doc.line(25, currentHeight, 185, currentHeight);

        currentHeight += 7;

        doc.setFontSize(12);
        doc.setTextColor(160, 205, 215);
        doc.text(115, currentHeight, 'SUBTOTAL');
        doc.setTextColor(65, 65, 65);
        doc.text(169, currentHeight, amountWithoutVat);


        currentHeight += 3;
        doc.setDrawColor(206, 215, 230);
        doc.setLineWidth(0.25);
        doc.line(113, currentHeight, 185, currentHeight);

        currentHeight += 6;
        doc.setTextColor(160, 205, 215);
        doc.text(115, currentHeight, 'VAT @ ' + this.showVatForGB(invoice) + '%');
        doc.setTextColor(65, 65, 65);
        doc.text(169, currentHeight, vatAmount);

        currentHeight += 3;
        doc.setDrawColor(120, 170, 180);
        doc.setLineWidth(0.25);
        doc.line(113, currentHeight, 185, currentHeight);

        currentHeight += 6;
        doc.setFontType('bold');
        doc.setTextColor(160, 205, 215);
        doc.text(115, currentHeight, 'TOTAL PAID');
        doc.setTextColor(65, 65, 65);
        doc.text(169, currentHeight, amountWithVat);

        // doc.setFontSize(13);
        // doc.setFontType('normal');
        // if (invoice.paid_date) {
        //     doc.text(196, 20, date, null, null, 'right');
        // }

        // doc.setFontType('normal');
        // doc.setFontSize(16);
        // doc.text(14, 30, 'Invoice Number: ' + invoice.invoice_id);
        //
        // doc.setFontType('bold');
        // doc.text(14, 45, 'From:');
        // doc.setFontType('normal');
        // doc.text(14, 51, 'Keith Richards');
        // doc.text(14, 57, '26 Beech Hill');
        // doc.text(14, 63, 'BarnetHerts EN4 0JP');
        // doc.text(14, 69, 'United Kingdom');

        // doc.setFontType('bold');
        // doc.text(196, 45, 'To:', null, null, 'right');
        // doc.setFontType('normal');
        // doc.text(196, 51, invoice.user.public_name, null, null, 'right');
        // if (invoice.user.location) {
        //     doc.text(196, 57, invoice.user.location, null, null, 'right');
        // }
        // if (invoice.user.phone) {
        //     doc.text(196, 63, invoice.user.phone, null, null, 'right');
        // }
        // if (invoice.user.email) {
        //     doc.text(196, 69, invoice.user.email, null, null, 'right');
        // }

        // payment received
        doc.setFontType('bold');
        doc.setFontSize(14);
        doc.setTextColor(65, 65, 65);
        doc.text(60, 250, 'PAYMENT RECEIVED WITH THANKS');
        // doc.text(196, 280, 'Keith Richards', null, null, 'right');

        // doc.autoTable(columns, rows, {margin: {top: 80}, styles: {fontSize: 15, font: 'arial'}});

        doc.save('invoice.pdf');
    }

    private dateFormat(value: any, formatForDate: string = '') {
        let momentDate: any = moment(value, 'DD-MM-YYYY');
        if (!momentDate.isValid()) {
          return value;
        }
        return momentDate.format(formatForDate);
    }

    private checkParams() {
        this.activatedRoute.params.subscribe((params: Params) => {
            if (params['id']) {
                this.openPDF(params['id']);
            }
        });
    }

}
